
data "terraform_remote_state" "vpc_eks" {
  backend = "http"

  config = {
    address  = var.http_state_address
    username = var.http_username
    password = var.http_access_token
  }
}
