variable "image_url" {
  default = "602401143452.dkr.ecr.us-east-1.amazonaws.com/amazon/aws-load-balancer-controller"
  # Please refer this for values for different region
}

variable "aws_region" {
  default = "us-east-1"
}

variable "product" {
  description = "The service/product that will be deployed on this resource/vpc"
}


# Environment 
variable "environment" {
  description = "The environment to which this vpc belong to (qa|prod|staging)"
  default     = "qa"
}

variable "alb_policy_json" {
  description = "URL of the IAM Policy required for AWS ALB Ingress Controller"
}

# For Remote Data Source
variable "http_state_address" {
  type        = string
  description = "Gitlab remote state file address"
}

variable "http_username" {
  type        = string
  description = "Gitlab username to query remote state"
}

variable "http_access_token" {
  type        = string
  description = "GitLab access token to query remote state"
}