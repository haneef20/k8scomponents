locals {
  name        = "${var.product}-${var.environment}"
  ecr_name    = "${local.name}-ecr"
  environment = var.environment
  product     = var.product
  common_tags = {
    environment = local.environment
    product     = var.product
  }
}
