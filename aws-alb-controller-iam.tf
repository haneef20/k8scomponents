## Download the IAM Policy˙
data "http" "lbc_iam_policy" {
  url = var.alb_policy_json

  # Optional request headers
  request_headers = {
    Accept = "application/json"
  }
}

# Resource: Create AWS Load Balancer Controller IAM Policy 
resource "aws_iam_policy" "lbc_iam_policy" {
  name        = "${local.name}-AWSLoadBalancerControllerIAMPolicy"
  path        = "/"
  description = "AWS Load Balancer Controller IAM Policy"
  policy      = data.http.lbc_iam_policy.response_body
  tags = merge(
    local.common_tags,
    {
      Name = "${local.name}-lbc-iam-policy"
    }
  )
}

# Resource: Create IAM Role 
resource "aws_iam_role" "lbc_iam_role" {
  name = "${local.name}-lbc-iam-role"

  # Terraform's "jsonencode" function converts a Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = data.terraform_remote_state.vpc_eks.outputs.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
            "${data.terraform_remote_state.vpc_eks.outputs.oidc_provider_short_arn}:sub" : "system:serviceaccount:kube-system:aws-load-balancer-controller"
          }
        }
      },
    ]
  })

  tags = merge(
    local.common_tags,
    {
      Name = "${local.name}-alb-iam-role"
    }
  )
}

# Associate Load Balanacer Controller IAM Policy to  IAM Role
resource "aws_iam_role_policy_attachment" "lbc_iam_role_policy_attach" {
  policy_arn = aws_iam_policy.lbc_iam_policy.arn
  role       = aws_iam_role.lbc_iam_role.name
}

