terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~>2.12.1"
    }
  }
backend "http" {
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.vpc_eks.outputs.cluster_endpoint
    cluster_ca_certificate = base64decode(data.terraform_remote_state.vpc_eks.outputs.cluster_certificate_authority_data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", data.terraform_remote_state.vpc_eks.outputs.cluster_name]
      command     = "aws"
    }
  }
}