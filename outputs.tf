output "repository_arn" {
    value = module.ecr.repository_arn
    sensitive = true
}


output "repository_url" {
    value = module.ecr.repository_url
    sensitive = true
}