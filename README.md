## Terraform Repo that sets up EKS Components and AWS Services required for Application Deployment on EKS

#### Description

This repository is used to create additional components on existing EKS and AWS to support application deployment . Details of the Components it sets up.

#### 1. AWS ALB Load Balancer Controller.
AWS ALB Controller will enable us to set AWS Applicaton Load Balancer based on the ingress resource. 

#### 2. Cluster Autoscaler
Cluster Autoscaler helps us to scale up and down the worker nodes based on the following conditions
* There are pods that failed to run in the cluster due to insufficient resources.
* There are nodes in the cluster that have been underutilized for an extended period of time and their pods can be placed on other existing nodes.

#### 3. Argocd 
Argocd is a gitops Continuous Delivery agent. This is used to facilitate application deployment in a gitops fashion. The basic idea is the ci tool in our case gitlab will build the application container image and update the kubernetes manifest repo when there are any changes. Argocd which runs on the cluster and monitors the repo sync the chanes either manually or automatically based on the configuration.
* ``` Argocd-apps``` - Once the argocd is installed on Cluster using argocd helm release. we use a argocd-apps helm chart ```https://artifacthub.io/packages/helm/argo/argocd-apps``` to setup a parent application on argocd using terraform. This parent appliction set on argocd will se the actual application on the argocd using Application Resource present in the application repo. This is combined with the repo ``` https://gitlab.com/haneef20/eksapp.git ```.

Argocd on the browser can be accessed using following the below
```
aws eks update-kubeconfig --region <aws-region> --name <cluster-name>

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

kubectl port-forward svc/argocd-server -n argocd 8080:443
```
We can access it on browser using ```http://localhost:8080``` and using the username ```admin``` and password obtained from the abovve.


#### 4. ECR 
We will use ECR as our container registry. So once the application image is built gitlab will run a job to push the image to ECR. We are setting up a private registry and also a lifecycle policy to get rid of old images based on the count.

#### CI/CD Details
We need an image that has aws,terraform and helm installed as the repo uses these providers to install applications on the cluster. 
To facilitate this custom image is setup and the details can be found in ```https://gitlab.com/haneef20/glabrunner.git ```


#### Usage Instructions
The repo sets up components on EKS so we need to make sure we have an EKS Cluster up and running. Also this repo  needs to have information to connect  cluster so it can connect to it to  install the required applications. This is faciliated using remote datasource. To run this locally please execute below.

```
terraform validate
terraform plan 
terraform apply
```
    

## Inputs

| Name | Description | Type | Default | Required |
| ---- | ----------- | ---- | ------- | -------- |
| aws_region| The region to deploy the resources to . | string | us-east-1 | no |
| environment | Usually of 'development’, ‘test’, or ‘production’ | string | qa | no |
| product | The product the infra is associated with  | string | none | yes |
| http_state_address | Gitlab remote state file address | string | no | yes |
| http_access_token  | Gitlab project access token required to access remote data source | string | no | yes
| http_username | Gitlab username to acess remote data source | string | no | yes


## Outputs VPC 

| Name | Description 
| ---- | ----------- 
| repository_arn | The Amazon Resource name of ECR repository created |
| repository_url | The URL of ECR repository created |


#### Enhancements and  Considerations.
* Karpenter could be a better replacement for cluster autoscaler so should look into it.
* Installing AWS ALB Ingress controller could also be done using EKS Addons.
* Can consider setting up argocd service as LoadBalancer so we dont have to port-forward to access the UI .









